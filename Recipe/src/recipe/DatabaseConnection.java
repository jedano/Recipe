/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recipe;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author student
 */
public class DatabaseConnection {

    public static Connection Connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = null;
            try {
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/recipe", "root", "");
                String sql = "INSERT INTO `country`(`countryName`, `capital`, `language`) VALUES (?,?,?)";
                Statement stat = conn.createStatement();
                ResultSet rs = stat.executeQuery("select * from country");
                System.out.println(String.format("%-15s%-15s%-15s%-15s", "CountryID", "Country", "Capital City", "Language"));
                while (rs.next()) {
                    System.out.println(String.format("%-15s%-15s%-15s%-15s", rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
            return conn;
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
}
