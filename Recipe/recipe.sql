-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2018 at 03:53 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recipe`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `countryID` smallint(5) UNSIGNED NOT NULL,
  `countryName` varchar(50) NOT NULL,
  `capital` varchar(50) NOT NULL,
  `language` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`countryID`, `countryName`, `capital`, `language`) VALUES
(1, 'Philippines', 'Manila', 'Tagalog'),
(2, 'India', 'Delhi', 'Hindi'),
(3, 'Thailand', 'Bangkok', 'Ambot'),
(4, 'Japan', 'Tokyo', 'Nihonggo');

-- --------------------------------------------------------

--
-- Table structure for table `ingredient`
--

CREATE TABLE `ingredient` (
  `ingredientID` smallint(5) UNSIGNED NOT NULL,
  `ingredientDescription` varchar(255) NOT NULL,
  `recipeID` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingredient`
--

INSERT INTO `ingredient` (`ingredientID`, `ingredientDescription`, `recipeID`) VALUES
(1, '2 skinless, boneless chicken breasts', 1),
(2, '1 teaspoon dried oregano', 1),
(3, '1/4 teaspoon cayenne pepper', 1),
(4, '1 pinch salt and ground black pepper to taste', 1),
(5, '1 tablespoon butter', 1),
(6, '1 (8 ounce) package chopped white mushrooms', 1),
(7, '1/4 cup chopped white onion', 1),
(8, '1 tablespoon Worcestershire sauce', 1),
(9, '1 pinch salt and cracked black pepper to taste', 1),
(10, '1 splash red wine', 1),
(11, '1/2 cup shredded mozzarella cheese', 1),
(12, '1 tablespoon chopped fresh parsley, for garnish', 2),
(13, '1/2 cup all-purpose flour', 2),
(14, '1 teaspoon salt', 2),
(15, '1/2 teaspoon paprika', 2),
(16, '1 egg', 2),
(17, '2 tablespoons milk', 2),
(18, '6 skinless, boneless chicken breast halves', 2),
(19, '4 tablespoons butter', 2),
(20, '1/2 pound fresh mushrooms, sliced', 2),
(21, '1/4 cup chopped onion', 2),
(22, '1 cup chicken broth', 2),
(23, '1/2 cup white wine', 2),
(24, '2 tablespoons lemon juice', 2),
(25, '1 tablespoon cornstarch', 2),
(26, '3/4 cup panko bread crumbs, or as needed', 3),
(27, 'cooking spray', 3),
(28, '1 cup mayonnaise', 3),
(29, '1 teaspoon paprika', 3),
(30, '3/4 cup panko bread crumbs, or as needed', 3),
(31, '1 teaspoon garlic powder', 3),
(32, '1 (14.75 ounce) can salmon, drained and flaked', 4),
(33, '1 (7.5 ounce) can salmon, drained and flaked', 4),
(34, '1 (4 ounce) packet saltine crackers, crushed', 4),
(35, '2 eggs, beaten', 4),
(36, '2 tablespoons mayonnaise', 4),
(37, '1 1/2 teaspoons Worcestershire sauce', 4),
(38, '1 1/2 teaspoons fresh lemon juice', 4),
(39, '1 teaspoon red pepper flakes', 4),
(40, '1 teaspoon dry mustard', 4),
(41, '2 tablespoons vegetable oil, or as needed', 4);

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE `recipe` (
  `recipeID` smallint(5) UNSIGNED NOT NULL,
  `recipeName` varchar(50) NOT NULL,
  `countryID` smallint(5) UNSIGNED NOT NULL,
  `userID` smallint(5) UNSIGNED NOT NULL,
  `procedures` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`recipeID`, `recipeName`, `countryID`, `userID`, `procedures`) VALUES
(1, 'Smothered Chicken with Mushrooms', 1, 2, '1. Preheat the oven to 350 degrees F (175 degrees C).\n2. Coat both sides of chicken breasts with oregano and cayenne pepper and place on a baking sheet. Season with salt and pepper.\n3. Bake in the preheated oven for 15 minutes.\n4. Melt butter in a nonstick skillet over medium heat. Add mushrooms, onion, Worcestershire sauce, and red wine. Cook and stir until vegetables shrink down a bit, about 5 minutes. Season with salt and cracked black pepper. Reduce heat to medium-low to low; cook until tender, 10 to 15 minutes.\n5. Pour mushroom mixture on top of the chicken. Continue baking until chicken is no longer pink in the center and the juices run clear, about 8 minutes. An instant-read thermometer inserted into the center should read at least 165 degrees F (74 degrees C).\n6. Sprinkle mozzarella cheese on top of the chicken and cook until cheese is bubbly, about 5 minutes more.'),
(2, 'Mushroom Chicken Piccata', 2, 1, '1. In a shallow dish or bowl, mix together flour, salt and paprika. In a separate dish or bowl, mix together egg and milk. Dip chicken pieces in egg mixture, then in seasoned flour.\r\n2. In a large skillet, heat butter or margarine over medium-high heat. Saute chicken pieces until golden brown. Add mushrooms and onion and saute for 3 to 5 minutes.\r\n3. In a medium bowl combine the broth, wine, lemon juice and cornstarch. Mix together and pour mixture over chicken and mushrooms. Reduce heat to medium low and let chicken mixture simmer for 25 minutes or until chicken is cooked through and juices run clear. Sprinkle with parsley and serve.'),
(3, 'Crispy Panko Chicken Thighs', 3, 4, '1. Preheat oven to 375 degrees F (190 degrees C).Line a baking pan with aluminum foil; coat with cooking spray.\r\n2. Combine mayonnaise and paprika in a bowl; mix well.\r\n3. Mix panko and garlic powder together in another bowl.\r\n4. Spread mayonnaise-paprika mixture on each chicken thigh; press into panko mixture to coat. Place in the prepared baking pan; spritz thighs with cooking spray.\r\n5. Bake in the preheated oven until no longer pink at the bone and the juices run clear, about 1 hour. An instant-read thermometer inserted near the bone should read 165 degrees F (74 degrees C).'),
(4, 'Jim\'s Salmon Patties', 4, 3, '1. Mix both amounts of salmon, crushed crackers, eggs, mayonnaise, Worcestershire sauce, lemon juice, red pepper flakes, and dry mustard together in a bowl. Chill in refrigerator to allow flavors to combine, 30 minutes. Form mixture into 6 patties.\r\n2. Heat vegetable oil in a skillet over medium heat; fry the salmon patties until golden brown, about 4 minutes per side.');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_ID` smallint(5) UNSIGNED NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `middlename` varchar(20) DEFAULT NULL,
  `lastaname` varchar(20) NOT NULL,
  `birthdate` date NOT NULL,
  `address` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_ID`, `firstname`, `middlename`, `lastaname`, `birthdate`, `address`) VALUES
(1, 'Jomar', 'Quipte', 'Edano', '1998-12-05', 'San Remigio'),
(2, 'Arona', 'Belda', 'Cartilla', '1998-07-01', 'Dalaguete'),
(3, 'Maricar', 'Taguro', 'Agunod', '1997-01-09', 'CDO'),
(4, 'Anna May', 'Li', 'Tinaja', '1995-11-01', 'Bohol');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`countryID`);

--
-- Indexes for table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`ingredientID`),
  ADD KEY `fk_recipeID` (`recipeID`);

--
-- Indexes for table `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`recipeID`),
  ADD KEY `fk_contryID` (`countryID`),
  ADD KEY `fk_userID` (`userID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `countryID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `ingredientID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `recipeID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_ID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ingredient`
--
ALTER TABLE `ingredient`
  ADD CONSTRAINT `fk_recipeID` FOREIGN KEY (`recipeID`) REFERENCES `recipe` (`recipeID`);

--
-- Constraints for table `recipe`
--
ALTER TABLE `recipe`
  ADD CONSTRAINT `fk_contryID` FOREIGN KEY (`countryID`) REFERENCES `country` (`countryID`),
  ADD CONSTRAINT `fk_userID` FOREIGN KEY (`userID`) REFERENCES `user` (`user_ID`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
