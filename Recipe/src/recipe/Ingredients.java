/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recipe;

/**
 *
 * @author student
 */
public class Ingredients {
    private int ingredientsID;
    private int recipeID;
    private String quantity;
    private String ingredientsName;

    public Ingredients() {
    }

    public Ingredients(int ingredientsID, int recipeID, String quantity, String ingredientsName) {
        this.ingredientsID = ingredientsID;
        this.recipeID = recipeID;
        this.quantity = quantity;
        this.ingredientsName = ingredientsName;
    }

    public int getIngredientsID() {
        return ingredientsID;
    }

    public void setIngredientsID(int ingredientsID) {
        this.ingredientsID = ingredientsID;
    }

    public int getRecipeID() {
        return recipeID;
    }

    public void setRecipeID(int recipeID) {
        this.recipeID = recipeID;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getIngredientsName() {
        return ingredientsName;
    }

    public void setIngredientsName(String ingredientsName) {
        this.ingredientsName = ingredientsName;
    }
    
}
